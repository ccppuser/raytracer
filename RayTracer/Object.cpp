#include "Object.h"

// 오브젝트
Object::Object(const Material &material)
: m_mtMaterial(material)
{
}

Object::~Object(void)
{
}

const Material &Object::GetMaterial() const
{
	return m_mtMaterial;
}

// 구
Sphere::Sphere(const GPoint3 &center, const Material &material, double radius)
: Object(material)
, m_ptCenter(center)
, m_dblRadius(radius)
{
}

Sphere::~Sphere()
{
}

int Sphere::GetObjectType() const
{
	return OBJECT_SPHERE;
}

const GPoint3 &Sphere::GetCenter() const
{
	return m_ptCenter;
}

GPoint3 Sphere::GetIntersectionPoint(const GLine &ray) const
{
	// 사용 공식: t = -( u * v ) ± sqrt( ( u * v )^2 - ( |u|^2 - r^2 ) )

	// 광선과 구가 교차하는 시점을 구함
	GPoint3 p0, q;
	GVector3 u, v;
	p0 = ray.GetPt();
	q = this->m_ptCenter;
	u = p0 - q;	// u = p0 - q;
	v = ray.GetDir();

	double insideRoot = SQR( u * v ) - ( SQR(norm(u)) - SQR(this->m_dblRadius) );	// (u*v)^2 - (|u|^2 - r^2)
	// 루트 안의 결과가 음수이면 허근이므로 교차하지 않음
	if(insideRoot < 0.0)
		return GPoint3(0.0, 0.0, 0.0);

	// 근의 공식에 의해 생성되는 두가지 값 구함
	double t1 = -(u * v) + SQRT(insideRoot);
	double t2 = -(u * v) - SQRT(insideRoot);
	// 중근일 경우 무시함
	if(t1 == t2)
		return GPoint3(0.0, 0.0, 0.0);

	return ray.Eval( MIN(t1, t2) );	// l(t)
}

GVector3 Sphere::GetNormal(const GPoint3 &ptOnObject) const
{
	return (ptOnObject - m_ptCenter).Normalize();
}

// 점 광원
Light::Light(const GPoint3 &center, const Material &material, const GPoint3 &intensity, double radius)
: Sphere(center, material, radius)
, m_ptIntensity(intensity)
{
}

Light::~Light(void)
{
}

int Light::GetObjectType() const
{
	return OBJECT_LIGHT;
}

const GPoint3 &Light::GetIntensity() const
{
	return m_ptIntensity;
}

// 정육면체
Cube::Cube(const GPoint3 &center, const Material &material, double edge)
: Object(material)
, m_ptCenter(center)
, m_dblEdge(edge)
{
}

Cube::~Cube()
{
}

int Cube::GetObjectType() const
{
	return OBJECT_CUBE;
}

const GPoint3 &Cube::GetCenter() const
{
	return m_ptCenter;
}

GPoint3 Cube::GetIntersectionPoint(const GLine &ray) const
{
	return GPoint3(0.0, 0.0, 0.0);
}

GVector3 Cube::GetNormal(const GPoint3 &ptOnObject) const
{
	return GVector3(0.0, 0.0, 0.0);
}

// 정사각 평면
Plane::Plane(const GVector3 &normal, double distance, const Material &material)
: Object(material)
, m_vNormal(normal)
, m_dblDistance(distance)
{
	m_vNormal.Normalize();
}

Plane::~Plane()
{
}

int Plane::GetObjectType() const
{
	return OBJECT_PLANE;
}

GPoint3 Plane::GetIntersectionPoint(const GLine &ray) const
{
	// 사용 공식 : t = -( N * p + d ) / (N * v)

	double dot = ( m_vNormal * ray.GetDir() );
	if(dot == 0.0)
		return GPoint3(0.0, 0.0, 0.0);

	GVector3 P(ray.GetPt()[0], ray.GetPt()[1], ray.GetPt()[2]);
	double t = -( (m_vNormal * P) + m_dblDistance ) / dot;
	if(t <= 0.0)
		return GPoint3(0.0, 0.0, 0.0);

	return ray.Eval(t);
}

GVector3 Plane::GetNormal(const GPoint3 &ptOnObject) const
{
	return m_vNormal;
}
