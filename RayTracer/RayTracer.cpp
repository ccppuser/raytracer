#include "RayTracer.h"
#include <float.h>

RayTracer::RayTracer(int width, int height, int maxDepth)
: m_nWidth(width)
, m_nHeight(height)
, m_nMaxDepth(maxDepth)
, m_pScene(NULL)
{
}

RayTracer::~RayTracer(void)
{
}

bool RayTracer::SetScene(Scene *srcScene)
{
	if(m_pScene)
		return false;

	m_pScene = srcScene;

	return true;
}

bool RayTracer::Render(const char *outFileName)
{
	if(!m_pScene)
		return false;

	// 결과 이미지를 저장할 화소 배열 생성
	Pixel *pData = new Pixel[m_nWidth * m_nHeight];

	// 광선의 출발점 정의
	GPoint3 proj_ref_pt(0.0f, 0.0f, 20.0f);

	// 광선의 출발점으로부터 모든 화소에 대하여 광선을 추적
	for	(int j = 0; j < m_nHeight; ++j)
	{
		for (int i = 0; i < m_nWidth; ++i)
		{
			// 뷰포트 설정
			double x = -4.0 + (double)i / (double)(m_nWidth - 1) * 8.0;
			double y = 3.0 - (double)j / (double)(m_nHeight - 1) * 6.0;
			GPoint3 q(x, y, 15.0);

			// 광선 생성
			GLine ray(proj_ref_pt, q);

			GVector3 C;
			C = RayTrace(ray, 0);

			pData[j * m_nWidth + i].r = (unsigned char)(255 * MIN( C[0], 1.0 ));
			pData[j * m_nWidth + i].g = (unsigned char)(255 * MIN( C[1], 1.0 ));
			pData[j * m_nWidth + i].b = (unsigned char)(255 * MIN( C[2], 1.0 ));
		}

		if(j % 100 == 99)
			printf_s("The pixels in %d row have been drawn.\n", j);	// 한 행의 픽셀이 그려졌음
	}

	// 이미지 파일로 추출
	if(!CreateImage(pData, outFileName))
		return false;

	// 화소 배열 메모리 해제
	if(pData)
	{
		delete [] pData;
		pData = NULL;
	}

	return true;
}

GVector3 RayTracer::RayTrace(GLine ray, int depth)
{
	if (depth >= m_nMaxDepth)
		return GVector3(0.0, 0.0, 0.0);

	// 광선과 만나는 첫번째 객체와의 교차점 계산
	double leastDistance = FLT_MAX;
	GPoint3 nearestIntersectPoint;
	Object *pIntersectObject = NULL;
	leastDistance = this->CaculateIntersection(ray, &nearestIntersectPoint, pIntersectObject);

	// 교차점이 존재하지 않으면
	if(leastDistance == FLT_MAX)
		return GVector3(0.0, 0.0, 0.0);

	// 광선이 광원과 교차했다면
	if(pIntersectObject->GetObjectType() == OBJECT_LIGHT)
		return pIntersectObject->GetMaterial().GetColor();

	GVector3 C;
	// 색 계산
	C += this->CaculateColor(ray, nearestIntersectPoint, *pIntersectObject);

	// 반사광선 계산
	C += this->CaculateReflect(ray, nearestIntersectPoint, *pIntersectObject, depth);

	// 굴절광선 계산
	//C += this->CaculateRefract();

	return C;
}

double RayTracer::CaculateIntersection(const GLine &ray, GPoint3 *pNearestIntersectPoint, Object *&pIntersectObject)
{
	double leastDistance = FLT_MAX;
	for(int i = 0; i < m_pScene->ObjectNum(); ++i)
	{
		Object *pObject = m_pScene->ObjectAt(i);

		GPoint3 intersectPoint = pObject->GetIntersectionPoint(ray);
		// 광선과 객체의 교차점이 존재하는가
		if(intersectPoint != GPoint3(0.0, 0.0, 0.0))
		{
			double distance = dist( ray.GetPt(), intersectPoint );
			// 광선의 시작점과 가장 가까운 교차점 찾음
			if(distance < leastDistance)
			{
				leastDistance = distance;
				*pNearestIntersectPoint = intersectPoint;
				pIntersectObject = pObject;
			}
		}
	}

	return leastDistance;
}

GVector3 RayTracer::CaculateColor(const GLine &ray, const GPoint3 &intersectPoint, const Object &object)
{
	GVector3 C;
	for(int i = 0; i < m_pScene->ObjectNum(); ++i)
	{
		if(m_pScene->ObjectAt(i)->GetObjectType() == OBJECT_LIGHT)
		{
			Light *pLight = static_cast<Light *>(m_pScene->ObjectAt(i));

			for(int j = 0; j < m_pScene->ObjectNum(); ++j)
			{
				Object *pObject = m_pScene->ObjectAt(j);

				// 교차점에서 광원까지의 광선 사이에 교차하는 객체가 있으면 그림자 생성(안 그리기)
				if(pObject->GetObjectType() != OBJECT_LIGHT
					&& this->CaculateShadow(*pLight, intersectPoint, *pObject))
					break;

				// 교차점에서 조명모델을 이용한 정점의 색 계산
				C += this->CaculatePhong(*pLight, ray, intersectPoint, object);
			}
		}
	}

	return C;
}

bool RayTracer::CaculateShadow(const Light &light, const GPoint3 &intersectPoint, const Object &object)
{
	GLine ToLightRay(intersectPoint, light.GetCenter());	// 교차점에서 광원까지의 광선
	GPoint3 interruptPoint = object.GetIntersectionPoint(ToLightRay);
	if(interruptPoint != GPoint3(0.0, 0.0, 0.0))
	{
		if(dist(light.GetCenter(), intersectPoint) > dist(interruptPoint, intersectPoint))
		{
			return true;
		}
	}

	return false;
}

GVector3 RayTracer::CaculatePhong(const Light &light, const GLine &ray, const GPoint3 &intersectPoint, const Object &intersectObject)
{
	GVector3 C;

	double distance = dist(light.GetCenter(), intersectPoint);
	GVector3 L = (light.GetCenter() - intersectPoint).Normalize();	// 교차점에서 광원으로 향하는 벡터
	GVector3 N(intersectObject.GetNormal(intersectPoint));	// 교차점의 노말 벡터
	GVector3 V(ray.GetDir());
	GVector3 H = (V + L).Normalize();
	double theta = N * L;
	double halfway = N * H;
	GVector3 I = light.GetMaterial().GetColor()
		/ (light.GetIntensity()[0] + light.GetIntensity()[1] * distance + light.GetIntensity()[2] * SQR(distance));

	// add diffuse
	if(theta > 0.0)
	{
		C += (intersectObject.GetMaterial().GetDiffuse() * light.GetMaterial().GetDiffuse() * theta)
			* (intersectObject.GetMaterial().GetColor() + I);
	}
	// add specular
	if(halfway > 0.0)
	{
		C += (intersectObject.GetMaterial().GetSpecular() * light.GetMaterial().GetSpecular() * pow(halfway, 1.0))
			* (I);
	}

	return C;
}

GVector3 RayTracer::CaculateReflect(const GLine &ray, const GPoint3 &intersectionPoint, const Object &intersectObject, int depth)
{
	GVector3 N = intersectObject.GetNormal(intersectionPoint);
	GVector3 R = ray.GetDir() - ((2.0 * ray.GetDir()) * N) * N;

	GLine ray_reflect = GLine(intersectionPoint, intersectionPoint + R);
	return intersectObject.GetMaterial().GetReflection() * RayTrace(ray_reflect, depth + 1);
}

GVector3 RayTracer::CaculateRefract()
{
	/*GVector3 N = pIntersectObject->GetNormal(nearestIntersectPoint);
	double n1 = REFRACTION_AIR;
	double n2 = pIntersectObject->GetMaterial().GetRefraction();
	double cos1 = N * (-ray.GetDir());
	double insidecos2 = 1.0 - SQR(n1 / n2) * (1.0 - SQR(cos1));
	if(insidecos2 > 0.0)
	{
		GVector3 T = (n1 / n2) * ray.GetDir() - (SQRT(insidecos2) - (n1 / n2) * cos1) * N;
		GLine ray_refract(nearestIntersectPoint, T.Normalize());
		C += RayTrace(ray_refract, depth + 1);
	}*/
	return GVector3();
}

bool RayTracer::CreateImage(Pixel *data, const char *outFileName)
{
	// 이미지를 ppm 파일로 저장
	FILE *fp = NULL;
	if(fopen_s(&fp, outFileName, "wb") != 0)
		return false;

	fprintf(fp, "P6\n");
	fprintf(fp, "%d\n", m_nWidth);
	fprintf(fp, "%d\n", m_nHeight);
	fprintf(fp, "%d\n", 255);
	fwrite(data, sizeof(Pixel), m_nWidth * m_nHeight, fp);

	fclose(fp);

	return true;
}
