#include "Scene.h"

Scene::Scene(void)
{
}

Scene::~Scene(void)
{
	for(std::vector<Object *>::iterator itr = m_vecObject.begin(); itr != m_vecObject.end(); ++itr)
	{
		if((*itr))
			delete (*itr);
	}
	m_vecObject.clear();
}

void Scene::AddLight(const GPoint3 &center, const Material &material, double radius, const GPoint3 &intensity)
{
	m_vecObject.push_back(new Light(center, material, intensity, radius));
}

void Scene::AddSphere(const GPoint3 &position, const Material &material, double radius)
{
	m_vecObject.push_back(new Sphere(position, material, radius));
}

void Scene::AddCube(const GPoint3 &position, const Material &material, double edge)
{
	m_vecObject.push_back(new Cube(position, material, edge));
}

void Scene::AddPlane(const GVector3 &normal, double distance, const Material &material)
{
	m_vecObject.push_back(new Plane(normal, distance, material));
}

int Scene::ObjectNum()
{
	return (int)m_vecObject.size();
}

Object *Scene::ObjectAt(int index)
{
	return m_vecObject[index];
}
