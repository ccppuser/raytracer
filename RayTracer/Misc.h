#pragma once

#include "gmath.h"

#define REFRACTION_AIR 1.0003
#define REFRACTION_GLASS 1.52

// 오브젝트의 재질
class Material
{
public:
	Material(const GVector3 &color, double diffuse, double fSpecular, double reflection, double refraction = REFRACTION_AIR)
		: m_vColor(color), m_dblDiffuse(diffuse), m_dblSpecular(fSpecular), m_dblReflection(reflection), m_dblRefraction(refraction) {}
	~Material() {}

	const GVector3 &GetColor() const { return m_vColor; }
	double GetDiffuse() const { return m_dblDiffuse; }
	double GetSpecular() const { return m_dblSpecular; }
	double GetReflection() const { return m_dblReflection; }
	double GetRefraction() const { return m_dblRefraction; }

private:
	GVector3 m_vColor;
	double m_dblDiffuse;
	double m_dblSpecular;
	double m_dblReflection;
	double m_dblRefraction;
};

// 결과 이미지에 출력될 하나의 화소
struct Pixel
{
	unsigned char r;
	unsigned char g;
	unsigned char b;
};
