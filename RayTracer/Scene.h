#pragma once

#include "Object.h"
#include <vector>

// �������� ���� ���� ����
class Scene
{
public:
	Scene(void);
	~Scene(void);

	void AddLight(const GPoint3 &center, const Material &material, double radius, const GPoint3 &intensity);
	void AddSphere(const GPoint3 &position, const Material &material, double radius);
	void AddCube(const GPoint3 &position, const Material &material, double edge);
	void AddPlane(const GVector3 &normal, double distance, const Material &material);
	int ObjectNum();
	Object *ObjectAt(int index);

private:
	std::vector<Object *> m_vecObject;
};
