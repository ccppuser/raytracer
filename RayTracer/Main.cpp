#include <stdio.h>
#include <stdlib.h>
#include "RayTracer.h"
#include <time.h>

#define H 600
#define W 800
#define MAX_DEPTH 3

int main()
{
	// �� ����
	Scene *pScene = new Scene();
	// ����Ʈ
	pScene->AddLight(GPoint3(-7.0, 2.0, 7.0)	// ���� ��ġ
		, Material(GVector3(1.0, 1.0, 1.0), 0.5, 0.5, 0.5)
		, 0.2	// ���� ũ��
		, GPoint3(1.0, 1.0, 0.5));	// �Ÿ��� ���� ����(attenuation; Ia, Ib, Ic)
	pScene->AddLight(GPoint3(7.0, 2.0, 7.0)
		, Material(GVector3(1.0, 1.0, 1.0), 0.5, 0.5, 0.5)
		, 0.2
		, GPoint3(1.0, 1.0, 0.5));
	// ��
	pScene->AddSphere(GPoint3(-4.0, -4.0, -4.0)	// ��ü ��ġ
		, Material(GVector3(0.5, 0.0, 0.0), 0.5, 0.5, 0.5, REFRACTION_GLASS)	// ��ü ����
		, 2.0);	// �� ������
	pScene->AddSphere(GPoint3(0.0, -2.0, 0.0)
		, Material(GVector3(0.0, 0.5, 0.0), 0.5, 0.5, 0.5, REFRACTION_GLASS)
		, 2.0);
	pScene->AddSphere(GPoint3(4.0, 0.0, 4.0)
		, Material(GVector3(0.0, 0.0, 0.5), 0.5, 0.5, 0.5, REFRACTION_GLASS)
		, 2.0);
	// ���
	pScene->AddPlane(GVector3(0.0, 1.0, 0.0)	// �ٴ�
		, 10.0
		, Material(GVector3(0.0, 0.0, 0.3), 0.5, 0.3, 0.3));
	pScene->AddPlane(GVector3(0.0, 0.0, 1.0)	// ����
		, 10.0
		, Material(GVector3(0.0, 0.0, 0.3), 0.5, 0.3, 0.3));

	// ���� ����
	RayTracer *pRayTracer = new RayTracer(W, H, MAX_DEPTH);
	pRayTracer->SetScene(pScene);

	printf_s("Start rendering...\n\n");

	pRayTracer->Render("result.ppm");

	printf_s("\nEnd rendering... elapsed time: %f sec\n\n", (float)clock() / 1000.0f);

	if(pRayTracer)
		delete pRayTracer;
	if(pScene)
		delete pScene;

	return 0;
}
