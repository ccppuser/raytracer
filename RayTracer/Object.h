#pragma once

#include "gmath.h"
#include "Misc.h"

#define OBJECT_LIGHT 1
#define OBJECT_SPHERE 2
#define OBJECT_CUBE 3
#define OBJECT_PLANE 4

// 각종 객체의 추상 클래스
class Object
{
public:
	Object(const Material &material);
	~Object(void);

	const Material &GetMaterial() const;
	virtual int GetObjectType() const = 0;
	virtual GPoint3 GetIntersectionPoint(const GLine &ray) const = 0;
	virtual GVector3 GetNormal(const GPoint3 &ptOnObject) const = 0;

protected:
	Material m_mtMaterial;
};

// 구
class Sphere : public Object
{
public:
	Sphere(const GPoint3 &center, const Material &material, double radius);
	~Sphere(void);

	virtual int GetObjectType() const;
	virtual GPoint3 GetIntersectionPoint(const GLine &ray) const;
	virtual GVector3 GetNormal(const GPoint3 &ptOnObject) const;
	const GPoint3 &GetCenter() const;

private:
	GPoint3 m_ptCenter;
	double m_dblRadius;
};

// 점 광원
class Light : public Sphere
{
public:
	Light(const GPoint3 &center, const Material &material, const GPoint3 &intensity, double radius);
	~Light(void);

	virtual int GetObjectType() const;
	const GPoint3 &GetIntensity() const;

private:
	GPoint3 m_ptIntensity;	// Ia, Ib, Ic
};

// 정육면체(구현 안됨)
class Cube : public Object
{
public:
	Cube(const GPoint3 &position, const Material &material, double edge);
	~Cube(void);

	virtual int GetObjectType() const;
	virtual GPoint3 GetIntersectionPoint(const GLine &ray) const;
	virtual GVector3 GetNormal(const GPoint3 &ptOnObject) const;
	const GPoint3 &GetCenter() const;

private:
	GPoint3 m_ptCenter;
	double m_dblEdge;
};

// 정사각 평면
class Plane : public Object
{
public:
	Plane(const GVector3 &normal, double distance, const Material &material);
	~Plane(void);

	virtual int GetObjectType() const;
	virtual GPoint3 GetIntersectionPoint(const GLine &ray) const;
	virtual GVector3 GetNormal(const GPoint3 &ptOnObject) const;

private:
	GVector3 m_vNormal;
	double m_dblDistance;
};
