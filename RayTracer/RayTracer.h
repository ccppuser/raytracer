#pragma once

#include "Misc.h"
#include "Scene.h"

// Ray Tracing을 이용한 렌더러
class RayTracer
{
public:
	RayTracer(int width = 800, int height = 600, int maxDepth = 3);
	~RayTracer(void);

	bool SetScene(Scene *srcScene);
	bool Render(const char *outFileName);

private:
	GVector3 RayTrace(GLine ray, int depth);
	double CaculateIntersection(const GLine &ray, GPoint3 *pNearestIntersectPoint, Object *&pIntersectObject);
	GVector3 CaculateColor(const GLine &ray, const GPoint3 &intersectPoint, const Object &object);
	bool CaculateShadow(const Light &light, const GPoint3 &intersectPoint, const Object &object);
	GVector3 CaculatePhong(const Light &light, const GLine &ray, const GPoint3 &intersectPoint, const Object &intersectObject);
	GVector3 CaculateReflect(const GLine &ray, const GPoint3 &intersectPoint, const Object &intersectObject, int depth);
	GVector3 CaculateRefract();	// 구현 안됨
	bool CreateImage(Pixel *data, const char *outFileName);

private:
	int m_nWidth;	// 최종 렌더링할 이미지 너비
	int m_nHeight;	// 최종 렌더링할 이미지 높이
	int m_nMaxDepth;	// 광선의 최대 추적 횟수
	Scene *m_pScene;	// 광선 추적을 수행할 씬 정보
};
